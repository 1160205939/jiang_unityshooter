﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour{
    public GameObject[] hazards;
    public Vector3 spawnValues;
    public int hazardCount;
    public float spawnWait;
    public float startWait;
    public float waveWait;

    public Text scoreText;
    public Text restartText;
    public Text gameOverText;
    public Text FireRateText;
    public Text WinnerText;

    private bool gameOver;
    private bool restart;
    private bool boss;
    public bool isBossScene;
    private int score;
    public GameObject Barrier;
    private PlayerController playerController;

    void Start()
    {
        gameOver = false;
        restart = false;
        boss = false;
        restartText.text = "";
        gameOverText.text = "";
        FireRateText.text = "";
        if (WinnerText!=null)
            WinnerText.text = "";
        score = 0;
        UpdateScore();
        StartCoroutine(SpawnWaves());
        Barrier.SetActive(false);

        GameObject playerControllerObject = GameObject.FindWithTag("Player");
        if (playerControllerObject != null)
        {
            playerController = playerControllerObject.GetComponent<PlayerController>();
        }
        if (playerController == null)
        {
            Debug.Log("Cannot find 'PlayerController' script");
        }
    }

    void Update()
    {
        if (restart)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                SceneManager.LoadScene("Main");
            }
        }
        FireRateText.text = "Current FireRate: " + Mathf.Round(playerController.fireRate * 100.0f) * 0.01f;

            if (score >= 1000 && isBossScene== false)
            {
                Boss();
            }


        
            if (boss)
            {
                SceneManager.LoadScene("Boss");
            }

        }
    

    IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(startWait);
        while (true)
        {
            for (int i = 0; i < hazardCount; i++)
            {
                GameObject hazard = hazards[Random.Range(0, hazards.Length)];
                Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
                Quaternion spawnRotation = Quaternion.identity;
                Instantiate(hazard, spawnPosition, spawnRotation);
                yield return new WaitForSeconds(spawnWait);
            }
            yield return new WaitForSeconds(waveWait);

            if (gameOver)
            {
                restartText.text = "Press 'R' for Restart";
                restart = true;
                break;
            }
        }
    }

    public void AddScore(int newScoreValue)
    {
        score += newScoreValue;
        UpdateScore();
    }

    void UpdateScore()
    {
        scoreText.text = "Score: " + score;

    }

    public void GameOver()
    {
        gameOverText.text = "Game Over!";
        gameOver = true;
    }

    public void Win() {
        WinnerText.text = "Congrats you win!";
        gameOver = true;
    }

    public void Boss() {
        boss = true;
    }
}