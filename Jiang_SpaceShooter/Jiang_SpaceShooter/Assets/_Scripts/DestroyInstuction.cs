﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyInstuction : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Invoke("DestroyMe", 2.5f);
	}
	
	void DestroyMe () {
        gameObject.SetActive(false);
	}
}
