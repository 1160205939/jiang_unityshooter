﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossController : MonoBehaviour {
    public GameObject explosion;
    public int scoreValue;

    public Slider healthBar;
    int maxHealth = 200;
    int health;

    private GameController gameController;

    void Start()
    {
        health = maxHealth;
        UpdateHealthBar();

        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }
        if (gameController == null)
        {
            Debug.Log("Cannot find 'GameController' script");
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Boundary") || other.CompareTag("Enemy") || other.CompareTag("Astroids"))
        {
            return;
        }

        if (other.tag == "Player")
        {
            other.GetComponent<PlayerController>().TakeDamage(1);
            TakeDamage(1);
        }

        if (other.tag == "Barrier")
        {
            other.gameObject.SetActive(false);
            TakeDamage(1);
        }

        if (other.tag == "Laser")
        {
            Instantiate(explosion, transform.position, transform.rotation);
            Destroy(other.gameObject);
            TakeDamage(1);
        }

        /*
        gameController.AddScore(scoreValue);
        Destroy(other.gameObject);
        Destroy(gameObject);*/
    }

    public void TakeDamage(int damage)
    {
        health -= damage;
        UpdateHealthBar();

        if (health <= 0)
        {
            Death();
        }
    }

    void Death() {
        Instantiate(explosion, transform.position, transform.rotation);
        gameController.Win();
        Destroy(gameObject);
    }

    void UpdateHealthBar()
    {
        healthBar.value = health / (float)(maxHealth);
    }
}
