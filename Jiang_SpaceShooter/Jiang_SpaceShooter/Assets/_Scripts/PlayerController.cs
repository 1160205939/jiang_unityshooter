﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Boundary
{
    public float xMin, xMax, zMin, zMax;
}
public class PlayerController : MonoBehaviour
{
    public float speed;
    public float tilt;
    private Rigidbody rb;
    public Boundary boundary;

    public GameObject shot;
    public Transform[] shotSpawns;
    public float fireRate;
    private bool Fire;
    private float nextFire;

    public Slider healthBar;
    int maxHealth = 5;
    int health;

    public GameObject playerExplosion;
    private GameController gameController;
    public GameObject barrier;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        Fire = false;
        health = maxHealth;
        UpdateHealthBar();
        barrier.SetActive(true);

        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }
        if (gameController == null)
        {
            Debug.Log("Cannot find 'GameController' script");
        }
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            {
            Fire = !Fire; 
        }

        if (/*Input.GetButton("Fire1") &&*/ Time.time > nextFire)
        {
            if (Fire)
            {
                nextFire = Time.time + fireRate;
                foreach (var shotSpawn in shotSpawns)
                {
                    Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
                }
                GetComponent<AudioSource>().Play();
            }
        }
    }
    private void FixedUpdate()
    {
          float moveHorizontal = Input.GetAxis("Horizontal");
          float moveVertical = Input.GetAxis("Vertical");

          Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.velocity = movement * speed;
        rb.position = new Vector3
            (
                Mathf.Clamp(rb.position.x, boundary.xMin, boundary.xMax),
                0.0f,
                Mathf.Clamp(rb.position.z, boundary.zMin, boundary.zMax)
            );

        rb.rotation = Quaternion.Euler(0.0f, 0.0f, rb.velocity.x * -tilt);
    }

    public void TakeDamage(int dmg) {
        health -= dmg;
        UpdateHealthBar();

        if (health <= 0) {
            Death();

        }
    }
    void Death() {
        Instantiate(playerExplosion, transform.position, transform.rotation);
        gameController.GameOver();
        Destroy(gameObject);

    }

    void UpdateHealthBar() {
        healthBar.value = health / (float)(maxHealth);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Pickups")
        {
            barrier.SetActive(true);
        }
    }
}