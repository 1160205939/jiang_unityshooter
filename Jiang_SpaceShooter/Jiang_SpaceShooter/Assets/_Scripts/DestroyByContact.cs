﻿using UnityEngine;
using System.Collections;

public class DestroyByContact : MonoBehaviour
{
    public GameObject explosion;
    public GameObject Barrier;
    public int scoreValue;

    private GameController gameController;

    void Start()
    {
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }
        if (gameController == null)
        {
            Debug.Log("Cannot find 'GameController' script");
        }

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Boundary") || other.CompareTag("Enemy")||other.CompareTag("Boss"))
        {
            return;
        }


        if (explosion != null)
        {
            Instantiate(explosion, transform.position, transform.rotation);
        }

        if (other.tag == "Player")
        {
            other.GetComponent<PlayerController>().TakeDamage(1);
            Destroy(gameObject);
            gameController.AddScore(scoreValue);
        }

        if (other.tag == "Barrier")
        {
            gameController.AddScore(scoreValue);
            Destroy(gameObject);
            other.gameObject.SetActive(false);
        }

        if (other.tag == "Laser")
        {
            Destroy(gameObject);
            Destroy(other.gameObject);
            gameController.AddScore(scoreValue);
        }


        /*
        gameController.AddScore(scoreValue);
        Destroy(other.gameObject);
        Destroy(gameObject);*/
    }
}